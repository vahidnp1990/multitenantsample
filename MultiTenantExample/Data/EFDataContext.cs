﻿using Microsoft.EntityFrameworkCore;
using MultiTenantExample.Entities;
using MultiTenantExample.TenantSetting;

namespace MultiTenantExample.Data
{

    public class EFDataContext : DbContext
    {
        private string TenantId ;
        public EFDataContext(DbContextOptions<EFDataContext> options, ITenantFromUser fromUser) : base(options)
        {
            TenantId = fromUser.TenantId;
            Database.SetConnectionString(fromUser.ConnectionString);
            if (!Database.CanConnect())
                Database.Migrate();
        }
        public DbSet<Product> Product { get; set; }
        public void Save()
        {
            this.MarkWithDataKeyIfNeeded(TenantId);
            SaveChanges();
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>().Property<string>("TenantId");
            modelBuilder.Entity<Product>().HasQueryFilter(_ => EF.Property<string>(_, "TenantId") == TenantId);
            base.OnModelCreating(modelBuilder);
        }
    }
}
