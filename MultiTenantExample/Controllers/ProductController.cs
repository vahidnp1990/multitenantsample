﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using MultiTenantExample.Data;
using MultiTenantExample.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MultiTenantExample.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ProductController : ControllerBase
    {
        private readonly EFDataContext _context;
        public ProductController(EFDataContext context)
        {
            _context = context;

        }
        [HttpGet]
        public List<Product> Get()
        {
            return _context.Product.ToList();
        }

        [HttpGet("add")]
        public void Add()
        {
            var random = new Random();
            _context.Product.Add(new Product { Name = "p" + random.Next(), Price = random.Next() });
            _context.Save();
        }
    }
}
