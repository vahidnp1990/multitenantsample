﻿using System.Collections.Generic;
using System.Linq;

namespace MultiTenantExample.TenantSetting
{
    public class TenantConnections : ITenantConnections
    {
        private readonly List<ConnectionString> _connections;

        public TenantConnections(List<ConnectionString> connections)
        {
            _connections = connections;
        }
        public string GetConnectionString(string connectionNem)
        {
            return _connections.FirstOrDefault(_ => _.Name == connectionNem)?.Connection;
        }
       
    }
}
