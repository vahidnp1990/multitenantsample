﻿using System;
using System.Threading.Tasks;

namespace MultiTenantExample.TenantSetting
{
    public interface ITenantConnections
    {
        string GetConnectionString(string connectionNem);
    }
}
