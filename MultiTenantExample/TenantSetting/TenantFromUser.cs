﻿using Microsoft.AspNetCore.Http;
using static MultiTenantExample.TenantSetting.TenantConnections;

namespace MultiTenantExample.TenantSetting
{
    public class TenantFromUser : ITenantFromUser
    {
        public TenantFromUser(IHttpContextAccessor accessor, ITenantConnections connections)
        {
            TenantId = accessor.HttpContext.Request.GetQueryStringTenantIdFromUser();
            string connectionName = accessor.HttpContext.Request.GetQueryStringConnectionNameFromUser();

            if (string.IsNullOrWhiteSpace(connectionName))
                connectionName = "dbConnection";

            ConnectionString = connections.GetConnectionString(connectionName);
        }
        public string TenantId { get; }

        public string ConnectionString { get; }
    }

}
