﻿namespace MultiTenantExample.TenantSetting
{
    public interface ITenantFromUser
    {
        string TenantId { get; }
        string ConnectionString { get; }
    }
}
