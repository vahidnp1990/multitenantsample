﻿using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Security.Claims;

namespace MultiTenantExample.TenantSetting
{
    public static class TenantExtention
    {
        public static string GetAuthTenantIdFromUser(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.FirstOrDefault(_ => _.Type == "tenant-id").Value;
        }
        public static string GetAuthConnectionNameFromUser(this ClaimsPrincipal claimsPrincipal)
        {
            return claimsPrincipal.Claims.FirstOrDefault(_ => _.Type == "connection_name").Value;
        }
        public static string GetQueryStringTenantIdFromUser(this HttpRequest request)
        {
            return request.Query["tenant-id"];
        }
        public static string GetQueryStringConnectionNameFromUser(this HttpRequest request)
        {
            return request.Query["connection_name"];
        }
         public static void MarkWithDataKeyIfNeeded(this DbContext context, string tenantId)
         {
             foreach (var entityEntry in context.ChangeTracker.Entries()
                 .Where(e => e.State == EntityState.Added))
             {
                 entityEntry.Property("TenantId").CurrentValue = tenantId;
             }
         }
    }
}
