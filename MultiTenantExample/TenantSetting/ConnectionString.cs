﻿namespace MultiTenantExample.TenantSetting
{
    public class ConnectionString
    {
        public string Name { get; set; }
        public string Connection { get; set; }
    }
}
